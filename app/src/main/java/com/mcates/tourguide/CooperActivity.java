package com.mcates.tourguide;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class CooperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trail_list);

        // Make navbar and statusbar transparent in activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        final ArrayList<Trail> trails = new ArrayList<Trail>();

        // Add list items here
        trails.add(new Trail(R.drawable.intermediate, "Homestead"));
        trails.add(new Trail(R.drawable.intermediate, "Fox Run"));
        trails.add(new Trail(R.drawable.intermediate, "Face"));
        trails.add(new Trail(R.drawable.advanced, "Accelerator"));
        trails.add(new Trail(R.drawable.intermediate, "Skip's Run"));
        trails.add(new Trail(R.drawable.beginner, "Fir Street"));
        trails.add(new Trail(R.drawable.intermediate, "Sugar"));
        trails.add(new Trail(R.drawable.beginner, "Lasso"));

        // Create {@link ArrayAdapter} custom adapter
        TrailAdapter adapter = new TrailAdapter(this, trails);

        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trail trail = trails.get(position);
            }
        });

    }
}
