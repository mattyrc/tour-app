package com.mcates.tourguide;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Make statusbar background completely transparent
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        // Start the Mountains activity with invisible button
        Button resorts = findViewById(R.id.start_btn);
        resorts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mountainsIntent = new Intent(MainActivity.this, ResortsActivity.class);
                startActivity(mountainsIntent);
                // Remove the this splash activity from history
                finish();
            }
        });

    }
}
