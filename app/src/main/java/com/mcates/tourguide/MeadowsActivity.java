package com.mcates.tourguide;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MeadowsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trail_list);

        // Make navbar and statusbar transparent in activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        final ArrayList<Trail> trails = new ArrayList<Trail>();

        // Add list items here
        trails.add(new Trail(R.drawable.advanced, "Super Bowl"));
        trails.add(new Trail(R.drawable.advanced, "Clark Canyon"));
        trails.add(new Trail(R.drawable.advanced, "Heather Canyon"));
        trails.add(new Trail(R.drawable.advanced, "A-Zone To H.C."));
        trails.add(new Trail(R.drawable.intermediate, "Texas Trail"));
        trails.add(new Trail(R.drawable.intermediate, "Catacombs"));
        trails.add(new Trail(R.drawable.intermediate, "Gulch"));
        trails.add(new Trail(R.drawable.intermediate, "Arena"));
        trails.add(new Trail(R.drawable.intermediate, "Sunburst"));
        trails.add(new Trail(R.drawable.advanced, "Elevator"));
        trails.add(new Trail(R.drawable.advanced, "Lower Elevator"));
        trails.add(new Trail(R.drawable.intermediate, "Outer Limits"));
        trails.add(new Trail(R.drawable.intermediate, "Inner Limits"));
        trails.add(new Trail(R.drawable.intermediate, "Discovery"));
        trails.add(new Trail(R.drawable.intermediate, "Nettie\'s Run"));
        trails.add(new Trail(R.drawable.intermediate, "Shooting Star"));
        trails.add(new Trail(R.drawable.advanced, "Mercury"));
        trails.add(new Trail(R.drawable.advanced, "Apollo"));
        trails.add(new Trail(R.drawable.intermediate, "Boulevard"));
        trails.add(new Trail(R.drawable.intermediate, "Cascade Skiway"));
        trails.add(new Trail(R.drawable.intermediate, "Chunky Swirly"));
        trails.add(new Trail(R.drawable.intermediate, "Ridge Run"));
        trails.add(new Trail(R.drawable.intermediate, "Ridge Run"));
        trails.add(new Trail(R.drawable.advanced, "1 Bowl"));
        trails.add(new Trail(R.drawable.advanced, "1 1/2 Bowl"));
        trails.add(new Trail(R.drawable.advanced, "2 Bowl"));
        trails.add(new Trail(R.drawable.advanced, "3 Bowl"));
        trails.add(new Trail(R.drawable.advanced, "4 Bowl"));
        trails.add(new Trail(R.drawable.advanced, "5 Bowl"));
        trails.add(new Trail(R.drawable.advanced, "Marmot Ridge"));
        trails.add(new Trail(R.drawable.intermediate, "Dental Floss"));
        trails.add(new Trail(R.drawable.intermediate, "Gingivitis"));
        trails.add(new Trail(R.drawable.beginner, "Nates Way"));
        trails.add(new Trail(R.drawable.beginner, "Nates Other Way"));
        trails.add(new Trail(R.drawable.beginner, "Vista Ridge"));
        trails.add(new Trail(R.drawable.beginner, "Lucky Lady"));
        trails.add(new Trail(R.drawable.beginner, "My TV"));
        trails.add(new Trail(R.drawable.advanced, "I Hear Cars"));
        trails.add(new Trail(R.drawable.beginner, "Michell Creek"));
        trails.add(new Trail(R.drawable.park, "Firewood Park"));
        trails.add(new Trail(R.drawable.intermediate, "Nastar Hill"));
        trails.add(new Trail(R.drawable.park, "Mini Pipe"));
        trails.add(new Trail(R.drawable.park, "Super Pipe"));
        trails.add(new Trail(R.drawable.beginner, "South Canyon"));
        trails.add(new Trail(R.drawable.advanced, "Waterfall"));
        trails.add(new Trail(R.drawable.beginner, "Speedwell"));
        trails.add(new Trail(R.drawable.intermediate, "Beaver Tail"));
        trails.add(new Trail(R.drawable.park, "Shipyard"));
        trails.add(new Trail(R.drawable.beginner, "Whoopee"));
        trails.add(new Trail(R.drawable.advanced, "The Face"));
        trails.add(new Trail(R.drawable.advanced, "Lower Face"));
        trails.add(new Trail(R.drawable.advanced, "Show Off"));
        trails.add(new Trail(R.drawable.advanced, "Powder Keg"));
        trails.add(new Trail(R.drawable.advanced, "Jacob\'s Ladder"));
        trails.add(new Trail(R.drawable.intermediate, "Wee Bee Gee Bee"));
        trails.add(new Trail(R.drawable.advanced, "Middle Fork"));
        trails.add(new Trail(R.drawable.intermediate, "Inner Limits"));
        trails.add(new Trail(R.drawable.intermediate, "Discovery"));
        trails.add(new Trail(R.drawable.advanced, "Roach Bowl"));
        trails.add(new Trail(R.drawable.beginner, "North Canyon"));
        trails.add(new Trail(R.drawable.intermediate, "Eric\'s Corner"));
        trails.add(new Trail(R.drawable.intermediate, "Stadium"));
        trails.add(new Trail(R.drawable.advanced, "Lady Finger"));
        trails.add(new Trail(R.drawable.advanced, "Lady Slipper"));
        trails.add(new Trail(R.drawable.park, "Forest Park"));
        trails.add(new Trail(R.drawable.intermediate, "Pipeline"));
        trails.add(new Trail(R.drawable.advanced, "Titan"));
        trails.add(new Trail(R.drawable.intermediate, "Fillicum"));
        trails.add(new Trail(R.drawable.intermediate, "Voyager"));
        trails.add(new Trail(R.drawable.intermediate, "Gemini"));
        trails.add(new Trail(R.drawable.intermediate, "Kinnikinick"));
        trails.add(new Trail(R.drawable.intermediate, "Wy East"));
        trails.add(new Trail(R.drawable.intermediate, "Outrigger"));
        trails.add(new Trail(R.drawable.intermediate, "Lower Outrigger"));
        trails.add(new Trail(R.drawable.intermediate, "Breakaway"));
        trails.add(new Trail(R.drawable.advanced, "Private Reserve"));
        trails.add(new Trail(R.drawable.advanced, "Elk"));
        trails.add(new Trail(R.drawable.advanced, "Yoda"));
        trails.add(new Trail(R.drawable.advanced, "Picnic Rock"));
        trails.add(new Trail(R.drawable.advanced, "S&amp;R Cliffs"));
        trails.add(new Trail(R.drawable.intermediate, "Knot Hole"));
        trails.add(new Trail(R.drawable.beginner, "Skiway"));
        trails.add(new Trail(R.drawable.advanced, "Fright Trees"));
        trails.add(new Trail(R.drawable.advanced, "Willow"));
        trails.add(new Trail(R.drawable.advanced, "Skillit"));

        // Create {@link ArrayAdapter} custom adapter
        TrailAdapter adapter = new TrailAdapter(this, trails);

        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trail trail = trails.get(position);
            }
        });

    }


}
