package com.mcates.tourguide;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class ResortsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resorts);

        // Make navbar and statusbar transparent in activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        // Start TimberlineActivity
        Button timberline = findViewById(R.id.trailsBtnOne);
        timberline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent timberlineIntent = new Intent(ResortsActivity.this, TimberlineActivity.class);
                startActivity(timberlineIntent);
            }
        });

        // Timberline Website Implicit Web link
        Button timberlineWeb = findViewById(R.id.webBtnOneWeb);
        timberlineWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.timberlinelodge.com/"));
                startActivity(browserIntent);
            }

        });

        // Start MeadowsActivity
        Button meadows = findViewById(R.id.trailsBtnTwo);
        meadows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent meadowsIntent = new Intent(ResortsActivity.this, MeadowsActivity.class);
                startActivity(meadowsIntent);
            }
        });

        // Meadows Website Implicit Web link
        Button meadowsWeb = findViewById(R.id.webBtnTwoWeb);
        meadowsWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.skihood.com/"));
                startActivity(browserIntent);
            }

        });

        // Start SkibowlActivity
        Button skibowl = findViewById(R.id.trailsBtnThree);
        skibowl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent skibowlIntent = new Intent(ResortsActivity.this, SkibowlActivity.class);
                startActivity(skibowlIntent);
            }
        });

        // Skibowl Website Implicit Web link
        Button skibowlWeb = findViewById(R.id.webBtnThreeWeb);
        skibowlWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.skibowl.com/winter/"));
                startActivity(browserIntent);
            }
        });

        // Start CooperActivity
        Button cooper =  findViewById(R.id.trailsBtnFour);
        cooper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cooperIntent = new Intent(ResortsActivity.this, CooperActivity.class);
                startActivity(cooperIntent);
            }
        });

        // Cooper Website Implicit Web link
        Button cooperWeb = findViewById(R.id.webBtnFourWeb);
        cooperWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.cooperspur.com/"));
                startActivity(browserIntent);
            }
        });

    }
}
