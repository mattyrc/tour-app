package com.mcates.tourguide;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class SkibowlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trail_list);

        // Make navbar and statusbar transparent in activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        final ArrayList<Trail> trails = new ArrayList<Trail>();

        trails.add(new Trail(R.drawable.advanced, "Pizza"));
        trails.add(new Trail(R.drawable.advanced, "Upper Canyon"));
        trails.add(new Trail(R.drawable.advanced, "Lower Canyon"));
        trails.add(new Trail(R.drawable.advanced, "Cannonball"));
        trails.add(new Trail(R.drawable.advanced, "Radical"));
        trails.add(new Trail(R.drawable.advanced, "Scotty's Way"));
        trails.add(new Trail(R.drawable.advanced, "Upper Reynolds"));
        trails.add(new Trail(R.drawable.beginner, "High Road"));
        trails.add(new Trail(R.drawable.advanced, "Wildfire"));
        trails.add(new Trail(R.drawable.advanced, "Middle Reynolds"));
        trails.add(new Trail(R.drawable.beginner, "Lower Reynolds"));
        trails.add(new Trail(R.drawable.beginner, "Low Road"));
        trails.add(new Trail(R.drawable.park, "Surprise"));
        trails.add(new Trail(R.drawable.park, "Easy Street"));
        trails.add(new Trail(R.drawable.beginner, "Broadway"));
        trails.add(new Trail(R.drawable.intermediate, "Mt. Hood Lane"));
        trails.add(new Trail(R.drawable.beginner, "Lower Bowl"));
        trails.add(new Trail(R.drawable.beginner, "Low Road"));
        trails.add(new Trail(R.drawable.beginner, "Roadhouse"));
        trails.add(new Trail(R.drawable.beginner, "Expressway"));
        trails.add(new Trail(R.drawable.intermediate, "Skidaddle"));
        trails.add(new Trail(R.drawable.advanced, "Challenger"));
        trails.add(new Trail(R.drawable.advanced, "Raceway"));
        trails.add(new Trail(R.drawable.advanced, "Yumper"));
        trails.add(new Trail(R.drawable.advanced, "Rhododendron"));
        trails.add(new Trail(R.drawable.advanced, "Cliffhanger"));
        trails.add(new Trail(R.drawable.advanced, "Calamity"));
        trails.add(new Trail(R.drawable.advanced, "Jane"));
        trails.add(new Trail(R.drawable.advanced, "Bob Strand\'s Downhill"));
        trails.add(new Trail(R.drawable.advanced, "Powder Keg"));
        trails.add(new Trail(R.drawable.advanced, "Upper Cutoff"));
        trails.add(new Trail(R.drawable.advanced, "Lower Cutoff"));
        trails.add(new Trail(R.drawable.advanced, "Outbreak 1"));
        trails.add(new Trail(R.drawable.advanced, "Outbreak 2"));
        trails.add(new Trail(R.drawable.advanced, "Outbreak 3"));
        trails.add(new Trail(R.drawable.advanced, "Tom Dick Bowl"));
        trails.add(new Trail(R.drawable.advanced, "The Chutes"));
        trails.add(new Trail(R.drawable.advanced, "West Boundary"));
        trails.add(new Trail(R.drawable.advanced, "Ruins of Bruin"));
        trails.add(new Trail(R.drawable.advanced, "Black Label Peak"));
        trails.add(new Trail(R.drawable.advanced, "Log Road"));
        trails.add(new Trail(R.drawable.intermediate, "Fire Hydrant"));
        trails.add(new Trail(R.drawable.intermediate, "Dog Leg"));
        trails.add(new Trail(R.drawable.beginner, "Lower Left"));
        trails.add(new Trail(R.drawable.advanced, "Dog Foot"));
        trails.add(new Trail(R.drawable.beginner, "Art\'s Canyon"));

        // Create {@link ArrayAdapter} custom adapter
        TrailAdapter adapter = new TrailAdapter(this, trails);

        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                Trail trail = trails.get(position);
            }
        });

    }

}
