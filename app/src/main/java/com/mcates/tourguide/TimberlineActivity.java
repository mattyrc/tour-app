package com.mcates.tourguide;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class TimberlineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trail_list);

        // Make navbar and statusbar transparent in activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        final ArrayList<Trail> trails = new ArrayList<Trail>();

        // Add list items here
        trails.add(new Trail(R.drawable.advanced, "Palmer"));
        trails.add(new Trail(R.drawable.advanced, "Willis"));
        trails.add(new Trail(R.drawable.advanced, "Bean\'s Run"));
        trails.add(new Trail(R.drawable.advanced, "Outer West"));
        trails.add(new Trail(R.drawable.intermediate, "Coffel\'s Run"));
        trails.add(new Trail(R.drawable.intermediate, "Kipp\'s Run"));
        trails.add(new Trail(R.drawable.intermediate, "Gordo\'s Mile"));
        trails.add(new Trail(R.drawable.intermediate, "Otto Lang"));
        trails.add(new Trail(R.drawable.intermediate, "Kruser"));
        trails.add(new Trail(R.drawable.park, "Spraypaint"));
        trails.add(new Trail(R.drawable.intermediate, "Norms"));
        trails.add(new Trail(R.drawable.beginner, "Lodge Getback"));
        trails.add(new Trail(R.drawable.park, "Conway's"));
        trails.add(new Trail(R.drawable.park, "The Bonezone"));
        trails.add(new Trail(R.drawable.park, "Blossom"));
        trails.add(new Trail(R.drawable.beginner, "Glade"));
        trails.add(new Trail(R.drawable.beginner, "Nona\'s Bologna"));
        trails.add(new Trail(R.drawable.intermediate, "Alpine"));
        trails.add(new Trail(R.drawable.intermediate, "Mustang Sally"));
        trails.add(new Trail(R.drawable.beginner, "West Leg Road"));
        trails.add(new Trail(R.drawable.intermediate, "Philox"));
        trails.add(new Trail(R.drawable.intermediate, "Jojami"));
        trails.add(new Trail(R.drawable.beginner, "Walt\'s Baby"));
        trails.add(new Trail(R.drawable.intermediate, "Uncle Jon\'s Brand"));
        trails.add(new Trail(R.drawable.intermediate, "Pete\'s Plunder"));
        trails.add(new Trail(R.drawable.advanced, "Buzz Cut"));
        trails.add(new Trail(R.drawable.beginner, "EZ Way"));
        trails.add(new Trail(R.drawable.intermediate, "Brother Beau"));
        trails.add(new Trail(R.drawable.beginner, "Waterline"));
        trails.add(new Trail(R.drawable.park, "Schoolyard"));
        trails.add(new Trail(R.drawable.advanced, "Wingle\'s Wiggle"));
        trails.add(new Trail(R.drawable.advanced, "Bob Elmer"));
        trails.add(new Trail(R.drawable.intermediate, "Thunder"));
        trails.add(new Trail(R.drawable.advanced, "Wy\' East"));
        trails.add(new Trail(R.drawable.intermediate, "Back Way"));
        trails.add(new Trail(R.drawable.intermediate, "Vicky\'s Run"));
        trails.add(new Trail(R.drawable.advanced, "Huck Bowl"));
        trails.add(new Trail(R.drawable.advanced, "West Pitch"));
        trails.add(new Trail(R.drawable.advanced, "Cut Off"));
        trails.add(new Trail(R.drawable.advanced, "Molly\'s Run"));
        trails.add(new Trail(R.drawable.advanced, "Joszi"));
        trails.add(new Trail(R.drawable.intermediate, "Lift Line"));
        trails.add(new Trail(R.drawable.intermediate, "Main Run Pucci"));

        // Create {@link ArrayAdapter} custom adapter
        TrailAdapter adapter = new TrailAdapter(this, trails);

        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trail trail = trails.get(position);
            }
        });


    }
}
