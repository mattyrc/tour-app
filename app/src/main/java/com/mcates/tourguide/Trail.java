package com.mcates.tourguide;

public class Trail {

    // Variables
    private String mTrailName;
    private int mImageResourceId = NO_IMAGE_PROVIDED;
    private static final int NO_IMAGE_PROVIDED = -1;

    /**
     * Create Object Constructor
     *
     * @param trailName
     * @param imageResourceId
     */
    public Trail(int imageResourceId, String trailName) {
        mImageResourceId = imageResourceId;
        mTrailName = trailName;
    }

    // Get the image resource Id
    public int getmImageResourceId() {
        return mImageResourceId;
    }

    // If there is an image in mImageResourceId the method
    // will return true.
    // If mImageResourceID is equal to -1, the there is no image for
    // the word and the method will return false.
    public boolean hasImage() {
        return mImageResourceId != NO_IMAGE_PROVIDED;
    }

    // Get the trail name
    public String getTrailName() {
        return mTrailName;
    }

}
