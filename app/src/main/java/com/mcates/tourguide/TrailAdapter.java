package com.mcates.tourguide;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TrailAdapter extends ArrayAdapter<Trail> {

    // Custom Constructor
    public TrailAdapter(Activity context, ArrayList<Trail> trails) {
        super(context, 0, trails);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Get the {@link Trail} object located on this position in the list
        Trail currentTrail = getItem(position);

        // Find the TextView in the list_item.xml
        TextView trailTextView = listItemView.findViewById(R.id.trail_text_view);
        trailTextView.setText(currentTrail.getTrailName());

        ImageView trailImageView = listItemView.findViewById(R.id.image);
        if (currentTrail.hasImage()) {

            trailImageView.setImageResource(currentTrail.getmImageResourceId());
            trailImageView.setVisibility(View.VISIBLE);
        } else {
            trailImageView.setVisibility(View.GONE);
        }

        // Return the whole list_item layout so that it is shown in the ListView
        return listItemView;
    }

}
